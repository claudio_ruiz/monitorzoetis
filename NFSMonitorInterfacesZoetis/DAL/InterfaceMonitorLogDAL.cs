﻿using System;
using System.Data;
using System.Data.SqlClient;
//using NFS.InterfaceMonitor.Log;


namespace NFSMonitorInterfacesZoetis
{
    public class InterfaceMonitorLogDAL: BaseDAL
    {
        public String GetSalesIdByDelivery(string delivery, string labid)
        {
            String salesId = "";   

            using (SqlConnection connection =
                   new SqlConnection(this.GetInterfaceMonitorConnectionString()))
            {
                SqlCommand command = new SqlCommand("SPNFS_SELECT_SALESID_BY_DELIVERY", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = 300;

                command.Parameters.Add("@DELIVERY", SqlDbType.NVarChar, 50);
                command.Parameters.Add("@LABID", SqlDbType.NVarChar, 5);
                
                command.Parameters["@DELIVERY"].Value = delivery;
                command.Parameters["@LABID"].Value = labid;

                connection.Open();

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    IDataRecord record = (IDataRecord)reader;
           
                    salesId = (String)record["SALESID"]; // reader["Estado"].ToString().Trim();
                  
                }
                reader.Close();
                connection.Close();
                connection.Dispose();
            }
            return salesId;
        }

        public void InsertOutputInterface(  String interfaceId,
                                            String fileName,
                                            String WmsId,
                                            String externalId,
                                            int status,
                                            DateTime createdDate,
                                            String fromApp)
        {
            using (SqlConnection connection =
                 new SqlConnection(this.GetInterfaceMonitorConnectionString()))
            {
                SqlCommand command = new SqlCommand("SPNFS_INSERT_OUTPUT_INTERFACE", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = 300;

                command.Parameters.Add("@INTERFACEID", SqlDbType.NVarChar, 200);
                command.Parameters.Add("@FILENAME", SqlDbType.NVarChar, 200);
                command.Parameters.Add("@WMSID", SqlDbType.NVarChar, 50);
                command.Parameters.Add("@EXTERNALID", SqlDbType.NVarChar, 50);
                command.Parameters.Add("@STATUS", SqlDbType.Int);
                command.Parameters.Add("@CREATEDDATETIME", SqlDbType.DateTime);
                command.Parameters.Add("@CREATEDBY", SqlDbType.NVarChar, 50);

                command.Parameters["@INTERFACEID"].Value = interfaceId;
                command.Parameters["@FILENAME"].Value = fileName;
                command.Parameters["@WMSID"].Value = WmsId;
                command.Parameters["@EXTERNALID"].Value = externalId;
                command.Parameters["@STATUS"].Value = status;
                command.Parameters["@CREATEDDATETIME"].Value = createdDate;
                command.Parameters["@CREATEDBY"].Value = fromApp;

                connection.Open();

                SqlDataReader reader = command.ExecuteReader();

                reader.Close();
                connection.Close();
                connection.Dispose();
            }
        }
    }
}
