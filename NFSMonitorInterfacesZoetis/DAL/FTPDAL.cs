﻿using System;
using System.Collections.Generic;
using MonitorDTO;
using System.Data;
using System.Data.SqlClient;

namespace NFSMonitorInterfacesZoetis
{
    public class FTPDAL: BaseDAL
    {        
        public int GetCounterStatus( String name, String status)
        {
            List<FtpDTO> list = new List<FtpDTO>();
            FtpDTO lineDto ;
            
            using (SqlConnection connection =
                   new SqlConnection(this.GetConnectionString()))
            {
                SqlCommand command = new SqlCommand("SPFTP_SELECT_BDFTPSUBIDA0001", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = 300;

                command.Parameters.Add("@Id", SqlDbType.BigInt);
                command.Parameters.Add("@Nombre", SqlDbType.NVarChar,200);
                command.Parameters.Add("@Estado", SqlDbType.NVarChar, 3);                          

                command.Parameters["@Id"].Value = null;
                command.Parameters["@Nombre"].Value = name;
                command.Parameters["@Estado"].Value = status;

                connection.Open();

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    IDataRecord record = (IDataRecord)reader;
                    lineDto = new FtpDTO();

                    lineDto.Id = (Int64)record["Id"]; // Convert.ToInt64(reader["Id"].ToString().Trim());
                    lineDto.Ftp = (String)record["Ftp"]; // reader["Ftp"].ToString().Trim();
                    lineDto.Path = (String)record["Ruta"]; //reader["Ruta"].ToString().Trim();
                    lineDto.Name = (String)record["Nombre"]; //reader["Nombre"].ToString().Trim();
                    lineDto.Status = (String)record["Estado"]; // reader["Estado"].ToString().Trim();
                    lineDto.Obs = (String)record["Comentario"]; //reader["Comentario"].ToString().Trim();
                    lineDto.CrtdTime = (DateTime)record["HoraCreacion"]; //Convert.ToDateTime(reader["HoraCreacion"].ToString().Trim());
                    lineDto.SentDateTime = (DateTime)record["HoraProceso"]; //Convert.ToDateTime(reader["HoraProceso"].ToString().Trim());
                    lineDto.ProcessName = (String)record["NombreProceso"]; //reader["NombreProceso"].ToString().Trim();
                    
                    list.Add(lineDto);
                }
                reader.Close();
                connection.Close();
                connection.Dispose();
            }
            return list.Count;
        }

        public Boolean SetFtpRecord(FtpDTO dataFtp)
        {
            using (SqlConnection connection = new SqlConnection(this.GetConnectionString()))
            {
                SqlCommand command = new SqlCommand("SPFTP_INSERT_BDFTPSUBIDA0002", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@Ftp", SqlDbType.VarChar, 500);
                command.Parameters.Add("@Ruta", SqlDbType.VarChar, 500);
                command.Parameters.Add("@Nombre", SqlDbType.VarChar, 200);
                command.Parameters.Add("@Estado", SqlDbType.VarChar, 3);
                command.Parameters.Add("@Comentario", SqlDbType.VarChar,2000);
                command.Parameters.Add("@HoraCreacion", SqlDbType.DateTime);
                command.Parameters.Add("@HoraProceso", SqlDbType.DateTime);
                command.Parameters.Add("@NombreProceso", SqlDbType.VarChar, 50);

                command.Parameters["@Ftp"].Value = dataFtp.Ftp;
                command.Parameters["@Ruta"].Value = dataFtp.Path;
                command.Parameters["@Nombre"].Value = dataFtp.Name;
                command.Parameters["@Estado"].Value = dataFtp.Status;
                command.Parameters["@Comentario"].Value = dataFtp.Obs;
                command.Parameters["@HoraCreacion"].Value = DateTime.Now;
                command.Parameters["@HoraProceso"].Value = DateTime.Now;
                command.Parameters["@NombreProceso"].Value = dataFtp.ProcessName;
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    return false;
                }
                connection.Close();
                connection.Dispose();
            }
            return true;
        }

        public List<FtpDTO> GeFtpZoetisList(String name)
        {
            List<FtpDTO> list = new List<FtpDTO>();
            FtpDTO lineDto;

            using (SqlConnection connection =
                   new SqlConnection(this.GetConnectionString()))
            {
                SqlCommand command = new SqlCommand("NFSSPGETZOETISFILES", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = 300;
                
                command.Parameters.Add("@NAME", SqlDbType.NVarChar, 200);

                command.Parameters["@NAME"].Value = name;

                connection.Open();

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    IDataRecord record = (IDataRecord)reader;
                    lineDto = new FtpDTO();

                    lineDto.Id = (Int64)record["Id"];
                    lineDto.Ftp = (String)record["Ftp"];
                    lineDto.Path = (String)record["Ruta"]; 
                    lineDto.Name = (String)record["Nombre"]; 
                    lineDto.Status = (String)record["Estado"];
                    lineDto.Obs = (String)record["Comentario"];
                    lineDto.CrtdTime = (DateTime)record["HoraCreacion"]; 
                    lineDto.SentDateTime = (DateTime)record["HoraProceso"]; 

                    list.Add(lineDto);
                }
                reader.Close();
                connection.Close();
                connection.Dispose();
            }
            return list;
        }


    }
}
