﻿using System;
using System.Collections.Generic;
using System.Linq;
using NFSMonitorInterfacesZoetis.NFSseInterfaceConfiguration;  //AplicativoFacturacion.NFSseInterfaceConfiguration;


namespace NFSMonitorInterfacesZoetis
{
    public class ServicioConfiguracion
    {
        /// <summary>
        /// Listado de configuracion de interfacez
        /// </summary>
        /// <param name="_idInterface"></param>
        /// <returns></returns>
        public List<NFSclParamConfigFacturacionDTO> GetInterfaceConfiguration(int _idInterface = 1)
        {
            InterfaceConfigurationServiceClient _cliente = new InterfaceConfigurationServiceClient();
            List<NFSclParamConfigFacturacionDTO> _lista = new List<NFSclParamConfigFacturacionDTO>();
            try
            {
                _lista = _cliente.getInterfaceConfiguration(null, _idInterface).ToList();
                _cliente.Close();
                return _lista;
            }
            catch (Exception ex)
            {
                _cliente.Abort();
                return null;
            }
            return _lista;
        }

        /// <summary>
        /// Trae lista de xml para enviar a Zoetis 
        /// </summary>
        /// <returns></returns>
        public List<NFSclRutaZoetisDTO> GetListZoetis()
        {
            InterfaceConfigurationServiceClient _cliente = new InterfaceConfigurationServiceClient();
            List<NFSclRutaZoetisDTO> _lista = new List<NFSclRutaZoetisDTO>();
            try
            {
                _lista = _cliente.getListZoetis(null).ToList();
                _cliente.Close();
                return _lista;
            }
            catch (Exception ex)
            {
                _cliente.Abort();
                return null;
            }
            return _lista;
        }

        /// <summary>
        /// Trae lista de xml para enviar a Zoetis 
        /// </summary>
        /// <returns></returns>
        public bool updateListZoetis(Int64 _rec)
        {
            InterfaceConfigurationServiceClient _cliente = new InterfaceConfigurationServiceClient();
            bool enviado = false;
            try
            {
                _cliente.updateListZoetis(null, _rec);
                _cliente.Close();
                enviado = true;
            }
            catch (Exception ex)
            {
                _cliente.Abort();
                enviado = false;
            }
            return enviado;
        }



    }
}
