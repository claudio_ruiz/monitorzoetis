﻿using System.Configuration;

namespace NFSMonitorInterfacesZoetis
{
    public abstract class BaseDAL 
    {
        public string GetConnectionString()
        {
           return ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["connString"]].ConnectionString;
        }

        public string GetInterfaceMonitorConnectionString()
        {
            return ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["connStringInterfaceMonitor"]].ConnectionString;
        }
      
    }
}
