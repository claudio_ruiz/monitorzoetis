﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonitorDTO
{
    public class DateTimeDTO
    {
        public int Anio { get; set; }
        public int mes { get; set; }
        public int Dia { get; set; }
        public int Hora { get; set; }
        public int Minuto { get; set; }
        public int Segundo { get; set; }
        public int ID { get; set; }
    }
}
