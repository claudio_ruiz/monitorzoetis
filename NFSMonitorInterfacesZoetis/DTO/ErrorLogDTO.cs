﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NFSMonitorInterfacesZoetis
{
    public class ErrorLogDTO
    {
        public int IdLog { get; set; }

        public String Interface { get; set; }
        public String Archivo { get; set; }
        public String Excepcion { get; set; }
        public DateTime Fecha { get; set; }
 
    }
}
