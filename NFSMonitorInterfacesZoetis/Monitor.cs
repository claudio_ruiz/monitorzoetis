﻿using MonitorDTO;
using NFSMonitorInterfacesZoetis.NFSseInterfaceConfiguration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Timers;
using System.Windows.Forms;
using System.Xml;

namespace NFSMonitorInterfacesZoetis
{
    public partial class Monitor : Form
    {
        #region Hilos, Delegados y Variables  
        LogFile lf = new LogFile();
        static System.Timers.Timer hilo1 = new System.Timers.Timer();
        delegate void DisplayStatus(string msg);
        delegate void DisplayErrorDGV(List<ErrorLogDTO> list);
        delegate void DisplayFtpDGV(List<FtpDTO> list);
        delegate void CleanDisplay();
        delegate int GetIntervalTime();
        string wms = string.Empty;
        string delivery = string.Empty;
        int processMinutes = 1;
        const int minute = 60000;      
        #endregion
        public Monitor()
        {
            InitializeComponent();
            LoadComboBox();
        }

        /// <summary>
        /// Metodo Inicial de timer
        /// </summary>
        /// <param name="myObject">Objeto</param>
        /// <param name="myEventArgs">Evento</param>
        private void Init(object myObject, EventArgs myEventArgs)
        {
            SetLoadDocuments();
        }

        /// <summary>
        /// Obtiene el pedido de laboratorio desde el nodo "VBELN" en el archivo fisico de la confirmacion de envio
        /// </summary>
        /// <param name="file">nombre de archivo</param>
        /// <param name="typeFile">tipo de arhivo generado</param>
        private string GetOBDbyXmlFile(string file)
        {
            XmlDocument xmldoc = new XmlDocument();
            XmlNodeList xmlnode;
            int i = 0;
            FileStream fs = new FileStream(file, FileMode.Open, FileAccess.Read);
            xmldoc.Load(fs); xmlnode = xmldoc.GetElementsByTagName("VBELN");
            for (i = 0; i <= xmlnode.Count - 1; i++)
            {
                wms = xmlnode[i].ChildNodes.Item(0).InnerText.Trim();
            }
            return wms;
        }

        /// <summary>
        /// Carga Lista de archivos pendientes
        /// </summary>
        /// <param name=""></param>
        private void SetLoadDocuments()
        {
            ServicioConfiguracion webService = new ServicioConfiguracion();
            List<NFSclRutaZoetisDTO> xmlZoetisList = new List<NFSclRutaZoetisDTO>();
            xmlZoetisList = webService.GetListZoetis();

            this.Invoke(new DisplayStatus(Progreso), "INICIO DE PROCESO");
            if (xmlZoetisList != null)
            {
                this.Invoke(new DisplayStatus(Progreso), $"LISTA CON {xmlZoetisList.Count } ELEMENTOS");

                lock(hilo1)
                {

                    if (xmlZoetisList.Count > 0)
                    {
                        GetXmlFilePath(xmlZoetisList);
                    }
                    
                     GetXmlFileRemaining();
                     this.Invoke(new DisplayStatus(Progreso), "NO HAY ARCHIVOS AÚN");                    
                }                 
            }
            else
            {
                this.Invoke(new DisplayStatus(Progreso), "LISTA VACIA, REVISAR DATOS Y CONEXIÓN");
            }
        }

        /// <summary>
        /// Metodo de proceso de XML 
        /// </summary>
        /// <param name="conf">Objeto archivo</param>
        private void GetXmlFilePath(List<NFSclRutaZoetisDTO> list)
        {
            FtpDTO ftpItem = new FtpDTO();
            FTPDAL ftpDal;
            string rutaCompleta = "";
            string nombreArchivoFinal = "";
            string rutaInicial;
            string type = "";
            int qtyErrorFiles = 0;
            int qtyCreatedFiles = 0;
            int qtyLoadedFiles = 0;
            int qtyProcessingFiles = 0;

            try
            {
                foreach (var item in list)
                {
                    if (item.Ruta != "")
                    {
                        if (File.Exists(item.Ruta))
                        {
                            type = item.Tipo;
                            rutaCompleta = item.Ruta;
                            string[] words = rutaCompleta.Split('\\');
                            nombreArchivoFinal = words[words.Length - 1];
                            rutaInicial = rutaCompleta.Replace(nombreArchivoFinal, "");
                            this.Invoke(new DisplayStatus(Progreso), "CONECTANDO AL MONITOR FTP " + nombreArchivoFinal);

                            ftpItem.Ftp = item.FTP;
                            ftpItem.Path = rutaInicial;
                            ftpItem.Name = nombreArchivoFinal;
                            ftpItem.Status = "CRE";
                            ftpItem.ProcessName = item.Proceso;
                            ftpItem.Obs = item.Comentario;

                            ftpDal = new FTPDAL();
                            qtyErrorFiles = ftpDal.GetCounterStatus(ftpItem.Name, "ERR");
                            qtyCreatedFiles = ftpDal.GetCounterStatus(ftpItem.Name, "CRE");
                            qtyProcessingFiles = ftpDal.GetCounterStatus(ftpItem.Name, "PUP");
                            qtyLoadedFiles = ftpDal.GetCounterStatus(ftpItem.Name, "OKU");
                            this.Invoke(new DisplayStatus(Progreso), $"TIPO {item.Tipo} INTERFACE ");

                            if (qtyErrorFiles > 0 || qtyCreatedFiles > 0 || qtyProcessingFiles > 0 || qtyLoadedFiles > 0)
                            {
                                if (qtyErrorFiles > 0)
                                {
                                    this.Invoke(new DisplayStatus(Progreso), $"ERROR CON: {ftpItem.Name }");
                                }

                                if (qtyCreatedFiles > 0)
                                {
                                    this.Invoke(new DisplayStatus(Progreso), $"CREADO: {ftpItem.Name }");
                                }

                                if (qtyProcessingFiles > 0)
                                {
                                    this.Invoke(new DisplayStatus(Progreso), $"PENDIENTEDE ENVÍO: {ftpItem.Name }");
                                }

                                if (qtyLoadedFiles > 0)
                                {
                                    this.Invoke(new DisplayStatus(Progreso), "ARCHIVO " + nombreArchivoFinal + " FUE SUBIDO CORRECTAMENTE");
                                    MoveFile(item);
                                }

                                continue;
                            }
                            else
                            {
                                #region Inserta LOG de Zoetis  
                                if (item.Tipo == "ZIDS0005")
                                {
                                    InterfaceMonitorLogDAL interfaceMon;
                                    delivery = GetOBDbyXmlFile(item.Ruta);
                                    this.Invoke(new DisplayStatus(Progreso), $"N° {delivery} LABORATORIO");
                                    interfaceMon = new InterfaceMonitorLogDAL();
                                    wms = interfaceMon.GetSalesIdByDelivery(delivery, "76");
                                    interfaceMon.InsertOutputInterface("6A61FCBE-5675-4861-A502-EE39C469ADAB",
                                                                       ftpItem.Name,
                                                                       wms,
                                                                       delivery,
                                                                       0,// <<<< STATUS
                                                                       DateTime.Now,
                                                                       "FTP_MONITOR");
                                    this.Invoke(new DisplayStatus(Progreso), "ID " + wms + " GENERADO PARA " + ftpItem.Name);
                                }
                                #endregion
                                ftpDal.SetFtpRecord(ftpItem);
                            }
                        }
                        else
                        {
                            this.Invoke(new DisplayStatus(Progreso), "ARCHIVO " + nombreArchivoFinal + " FUE SUBIDO CORRECTAMENTE");
                            GetXmlFileRemaining();
                        }                       
                    }
                }
            }
            catch (Exception ex)
            {
                this.Invoke(new DisplayStatus(Progreso), "ERROR:  " +DateTime.Now.ToString() + nombreArchivoFinal + " "+ex.Message);
            }
        }

        /// <summary>
        /// Metodo de proceso de XML 
        /// </summary>
        /// <param name="conf">Objeto archivo</param>
        private void GetXmlFileRemaining()
        {
            FtpDTO ftpItem = new FtpDTO();
            FTPDAL ftpDal;
            string rutaCompleta = "";
            string nombreArchivoFinal = "";
          
            int qtyErrorFiles = 0;
            int qtyCreatedFiles = 0;
            int qtyLoadedFiles = 0;
            int qtyProcessingFiles = 0;        

            try
            {
                string rutaInicial = ConfigurationManager.AppSettings["rutaInicial"].ToString();
                string rutaFinal = ConfigurationManager.AppSettings["rutaFinal"].ToString();
                string[] pdfFiles = GetFileNames(rutaInicial, "*.xml");// RESCATA LOS ARCHIVOS XML      
               
                foreach (string  item in pdfFiles)
                {
                        string[] tipo = item.Split('_');
                        rutaCompleta = rutaInicial + "\\"+ item;
                        string[] tipoArray= rutaCompleta.Split('\\');
                        string[] words = rutaCompleta.Split('\\');
                        nombreArchivoFinal = words[words.Length - 1];
                        rutaInicial = rutaCompleta.Replace(nombreArchivoFinal, "");
                        this.Invoke(new DisplayStatus(Progreso), "CONECTANDO AL MONITOR FTP " + nombreArchivoFinal);

                        ftpItem.Ftp = "76IREQUP";
                        ftpItem.Path = rutaInicial;
                        ftpItem.Name = nombreArchivoFinal;
                        ftpItem.Status = "CRE";
                        ftpItem.ProcessName = "Insercion de datos Zoetis";
                        ftpItem.Obs = "Archivo para enviar de laboratorio Zoetis";

                        ftpDal = new FTPDAL();
                        qtyErrorFiles = ftpDal.GetCounterStatus(ftpItem.Name, "ERR");
                        qtyCreatedFiles = ftpDal.GetCounterStatus(ftpItem.Name, "CRE");
                        qtyProcessingFiles = ftpDal.GetCounterStatus(ftpItem.Name, "PUP");
                        qtyLoadedFiles = ftpDal.GetCounterStatus(ftpItem.Name, "OKU");
                        this.Invoke(new DisplayStatus(Progreso), $"TIPO {tipoArray[0]} INTERFACE ");

                        if (qtyErrorFiles > 0 || qtyCreatedFiles > 0 || qtyProcessingFiles > 0 || qtyLoadedFiles > 0)
                        {
                            if (qtyErrorFiles > 0)
                            {
                                this.Invoke(new DisplayStatus(Progreso), $"ERROR CON: {ftpItem.Name }");
                            }

                            if (qtyCreatedFiles > 0)
                            {
                                this.Invoke(new DisplayStatus(Progreso), $"CREADO: {ftpItem.Name }");
                            }

                            if (qtyProcessingFiles > 0)
                            {
                                this.Invoke(new DisplayStatus(Progreso), $"PENDIENTEDE ENVÍO: {ftpItem.Name }");
                            }

                            if (qtyLoadedFiles > 0)
                            {
                                this.Invoke(new DisplayStatus(Progreso), "ARCHIVO " + nombreArchivoFinal + " FUE SUBIDO CORRECTAMENTE");
                                NFSclRutaZoetisDTO conf = new NFSclRutaZoetisDTO ();
                                conf.Ruta = rutaCompleta;
                                conf.RutaParcial = rutaFinal;
                                conf.Rec = "0";
                                MoveFile(conf);
                            }
                            continue;
                        }
                        else
                        {
                           ftpDal.SetFtpRecord(ftpItem);
                        }
                }
            }
            catch (Exception ex)
            {
                this.Invoke(new DisplayStatus(Progreso), "ERROR:  " + DateTime.Now.ToString() + nombreArchivoFinal + " " + ex.Message);
            }
        }

        /// <summary>
        /// Metodo que nuevo a carpeta de respaldo los xml enviados 
        /// </summary>
        /// <param name="conf">Objeto archivo</param>
        private void MoveFile(NFSclRutaZoetisDTO conf)
        {
            string nombreArchivoFinal="";
            try
            {
                ServicioConfiguracion innerService = new ServicioConfiguracion();
                string rutaRespaldo;
                string rutaCompleta = conf.Ruta;
             
                string rutaInicial;
                string rec = conf.Rec;
                try
                {
                    string[] words = rutaCompleta.Split('\\');
                    nombreArchivoFinal = words[words.Length - 1];
                    rutaInicial = rutaCompleta.Replace(nombreArchivoFinal, "");

                    try
                    {
                        rutaRespaldo = conf.RutaParcial + "\\RESPALDO\\" + conf.Tipo;

                        if (!Directory.Exists(rutaRespaldo))
                        {
                            this.Invoke(new DisplayStatus(Progreso), "DIRECTORIO RESPALDO " + rutaRespaldo + " NO EXISTE");
                            Directory.CreateDirectory(rutaRespaldo);
                            this.Invoke(new DisplayStatus(Progreso), "DIRECTORIO RESPALDO " + rutaRespaldo + " FUE CREADO CORRECTAMENTE");
                        }
                        this.Invoke(new DisplayStatus(Progreso), "BUSCANDO XML " + nombreArchivoFinal);
                        if (File.Exists(rutaCompleta))
                        {
                            FTPDAL ftpDal = new FTPDAL();
                            if (ftpDal.GetCounterStatus(nombreArchivoFinal, "OKU") > 0)
                            {
                                File.Move(rutaCompleta, rutaRespaldo + "\\" + nombreArchivoFinal);
                                innerService.updateListZoetis(Convert.ToInt64(conf.Rec));
                                this.Invoke(new DisplayStatus(Progreso), "XML " + nombreArchivoFinal + " RESPALDADO EN " + rutaRespaldo + " CORRECTAMENTE");
                            }
                            else
                            {
                                this.Invoke(new DisplayStatus(Progreso), "XML " + nombreArchivoFinal + " EN ESPERA");
                            }
                        }
                        else
                        {
                            innerService.updateListZoetis(Convert.ToInt64(conf.Rec));
                            this.Invoke(new DisplayStatus(Progreso), "XML " + nombreArchivoFinal + " YA FUE RESPALDADO");
                        }
                    }
                    catch (Exception ex)
                    {
                        this.Invoke(new DisplayStatus(Progreso), "ERROR AL USAR LA CLASE "+ DateTime.Now.ToString()+" " + ex.Message);
                    }
                }
                catch (Exception ex)
                {
                    this.Invoke(new DisplayStatus(Progreso), "ERROR AL ACTUALIZAR REGISTROS EN LA BASE DE DATOS " + conf.Ruta + " " + ex.Message);
                }
            }
            catch (Exception ex)
            {
                this.Invoke(new DisplayStatus(Progreso), "ERROR EN EL METODO MoveFile " + ex.Message);
            }
        }

        private void LoadDataGridFtp(List<FtpDTO> list)
        {
            try
            {
                string estado = cmbStatus.SelectedItem.ToString().ToUpper();
                estado = (estado.Equals("TODO")) ? "" : estado;
                dgvFtp.DataSource = null;

                if (list != null && list.Count > 0)
                {
                  dgvFtp.DataSource = (string.IsNullOrEmpty(estado)) ? list : list.Where(x => x.Status.Trim().ToUpper() == estado).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Invoke(new DisplayStatus(Progreso), "ERROR: " + DateTime.Now.ToString() + " " + ex.Message);

            }
        }

        private static string[] GetFileNames(string carpeta, string extencionDocto)// busca todos los xml de una carpeta
        {
            string[] files = Directory.GetFiles(carpeta, extencionDocto);
            for (int i = 0; i < files.Length; i++)
                files[i] = Path.GetFileName(files[i]);
            return files;
        }

        private void btn_start_Click(object sender, EventArgs e)
        {
            processMinutes = Convert.ToInt32(this.Invoke(new GetIntervalTime(GetTimeIteration)));
            btn_start.Enabled = false;
            hilo1.Enabled = true;
            hilo1.Start();         
            hilo1.Elapsed += new ElapsedEventHandler(Init);
            hilo1.Interval = processMinutes * minute;
        }

        /// <summary>
        /// Obtiene minutos de intervalo 
        /// </summary>
        /// <param name=""></para
        public int GetTimeIteration()
        {
            int intTime = Convert.ToInt32(numUpDwn.Value);
            return (intTime == 0) ? 1 : intTime;
        }

        /// <summary>
        /// Botón de cancelar
        /// </summary>
        /// <param name="sender">Objeto</param>
        /// <param name="EventArgs">Evento</param>|
        private void btn_cancel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Desea cancelar el proceso de actualización de FTP?", "Cancelación", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    #region
                    hilo1.Stop();
                    hilo1.Enabled = false;
                    hilo1.Dispose();
                    hilo1.Close();
                    #endregion
                }
                catch (Exception) { }
                btn_start.Enabled = true;
                this.Close();
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            this.Invoke(new CleanDisplay(clearScreen));
        }

        /// <summary>
        /// Setea minutos de intervalo
        /// </summary>
        /// <param name="sender">Objeto</param>
        /// <param name="EventArgs">Evento</param>
        private void numUpDwn_ValueChanged(object sender, EventArgs e)
        {
            processMinutes = Convert.ToInt32(this.Invoke(new GetIntervalTime(GetTimeIteration)));
        }

        /// <summary>
        /// Limpia la pantalla
        /// </summary>
        /// <param name=""></param>
        private void clearScreen()
        {
            txb_msg.Text = "";
        }

        /// <summary>
        /// Metodo que muestra log en visor de formulario
        /// </summary>
        /// <param name="msg">Mensaje a mostrar en pantalla</param>
        public void Progreso(string msg)
        {
            string enter = char.ConvertFromUtf32(13) + char.ConvertFromUtf32(10);
            string ant_Text = txb_msg.Text;
            if (ant_Text.Length > 1024)
            {
                this.Invoke(new CleanDisplay(clearScreen));
            }
            txb_msg.Text = msg + enter + ant_Text;        

            lf.WriteTxtLog("LOG_MONITOR_ZOETIS.TXT", ant_Text);
        }

        private void btnFtp_Click(object sender, EventArgs e)
        {
            FTPDAL f = new FTPDAL(); 
            string interfaz =cmbInterface.SelectedItem.ToString().ToUpper();
            interfaz = (interfaz.Equals("SELEC. INTERFACE")) ? "": interfaz + "%";       
            this.Invoke(new DisplayFtpDGV(LoadDataGridFtp), f.GeFtpZoetisList(interfaz));
        }

        private void LoadComboBox()
        {            
            cmbInterface.Items.Add("ZIDS0001");
            cmbInterface.Items.Add("ZIDS0002");
            cmbInterface.Items.Add("ZIDS0003");
            cmbInterface.Items.Add("ZIDS0004");
            cmbInterface.Items.Add("ZIDS0005");
            cmbInterface.Items.Add("ZIDS0006");
            cmbInterface.Items.Add("ZIDS0501");
            cmbInterface.Items.Add("ZIDS0008");
            cmbInterface.SelectedIndex = 0;
            cmbStatus.Items.Add("CRE");
            cmbStatus.Items.Add("ERR");
            cmbStatus.Items.Add("DPL");
            cmbStatus.Items.Add("FNE");
            cmbStatus.Items.Add("OKU");
            cmbStatus.SelectedIndex = 0;
        }

        private void dgvFtp_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            MessageBox.Show(this.dgvFtp.CurrentRow.Cells[e.ColumnIndex].Value.ToString(), "FTP");
        }

        private void btn_logs_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("explorer.exe", lf.GetPathLogProcessString()) ;
        }
    }
}
